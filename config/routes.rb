Rails.application.routes.draw do
  
  #Rotas de sessionscontroller
  get 'login', to: 'sessions#new', as: :login
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy', as: :logout

  #Rotas Usercontroller  
  get 'users', to: 'users#index', as: :users
  get 'users/new', to: 'users#new', as: :users_new
  post 'users/new', to: 'users#create'
  get 'users/:id', to: 'users#show', as: :user
  get 'users/edit/:id', to: 'users#edit', as: :users_edit
  #bomba patch 100% atualizado
  patch 'users/edit/:id', to: 'users#update'
  delete 'users/destroy/:id', to: 'users#destroy', as: :user_destroy
  
end
