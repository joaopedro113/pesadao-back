module SessionsHelper
    
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # Se houver alguem logado, retornara o usuario
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  # Se houver alguem logado, retorna true
  def logged_in?
    !current_user.nil?
  end
  
  # Retira o id do usuario logado do cookie de sessao e limpa o current user
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end


    

end
