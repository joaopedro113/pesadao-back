class UsersController < ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]
    before_action:user_params, only: [:create, :update]
    def new
        @user = User.new
    end
    
    def create
        @user = User.new(user_params)
        if @user.save
            redirect_to user_path(@user)
        else 
            render :new 
        end
    end
    
    def show
    end
    
    def edit 
    end
    
    def update
        if @user.update_attributes(user_params)
            redirect_to user_path(@user)
        else 
            render :edit
        end
    end
    
    def index
        #variaveis com @ sao as variaveis de instancia
        #ficam visiveis para views
        @users  = User.all
    end
    
    def destroy
        @user.destroy
        redirect_to users_path
    end
    
    private
    
    def set_user 
        @user = User.find(params[:id])
    end
    
    def user_params
        @user_params = params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
        
end
